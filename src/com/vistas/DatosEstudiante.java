
package com.vistas;

import com.clases.Docente;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;

/**
 * Nombre de la clase: DatosEstudiante
 * Fecha: 22/05/2018
 * Vrsion: 8.1
 * CopyRigth: Itca Fepade
 * @author kenya y Andrea
 */
public class DatosEstudiante extends javax.swing.JInternalFrame {

    /**
     * Creates new form DatosEstudiante
     */
    public DatosEstudiante() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jNombre = new javax.swing.JTextField();
        jCarnet = new javax.swing.JTextField();
        jPromTeoria = new javax.swing.JTextField();
        jPromPractica = new javax.swing.JTextField();
        jEnviar = new javax.swing.JButton();
        jLimpiar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setClosable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Ejercicio 5");
        setVisible(true);

        jLabel2.setFont(new java.awt.Font("Segoe UI Light", 0, 12)); // NOI18N
        jLabel2.setText("Carnet: ");

        jLabel3.setFont(new java.awt.Font("Segoe UI Light", 0, 12)); // NOI18N
        jLabel3.setText("Nombre:");

        jLabel4.setFont(new java.awt.Font("Segoe UI Light", 0, 12)); // NOI18N
        jLabel4.setText("Nota Promedio de teoria:");

        jLabel5.setFont(new java.awt.Font("Segoe UI Light", 0, 12)); // NOI18N
        jLabel5.setText("Nota promedio de practica: ");

        jNombre.setFont(new java.awt.Font("Segoe UI Light", 0, 10)); // NOI18N
        jNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jNombreActionPerformed(evt);
            }
        });
        jNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jNombreKeyTyped(evt);
            }
        });

        jCarnet.setFont(new java.awt.Font("Segoe UI Light", 0, 10)); // NOI18N
        jCarnet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCarnetActionPerformed(evt);
            }
        });
        jCarnet.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jCarnetKeyTyped(evt);
            }
        });

        jPromTeoria.setFont(new java.awt.Font("Segoe UI Light", 0, 10)); // NOI18N
        jPromTeoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jPromTeoriaActionPerformed(evt);
            }
        });
        jPromTeoria.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jPromTeoriaKeyTyped(evt);
            }
        });

        jPromPractica.setFont(new java.awt.Font("Segoe UI Light", 0, 10)); // NOI18N
        jPromPractica.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jPromPracticaKeyTyped(evt);
            }
        });

        jEnviar.setFont(new java.awt.Font("Segoe UI Light", 1, 12)); // NOI18N
        jEnviar.setText("Enviar");
        jEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jEnviarActionPerformed(evt);
            }
        });

        jLimpiar.setFont(new java.awt.Font("Segoe UI Light", 1, 12)); // NOI18N
        jLimpiar.setText("Limpiar");
        jLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jLimpiarActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI Light", 1, 14)); // NOI18N
        jLabel1.setText("Datos de estudiante: ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jEnviar)
                                .addGap(11, 11, 11)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jCarnet, javax.swing.GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE)
                                    .addComponent(jNombre)
                                    .addComponent(jPromTeoria)
                                    .addComponent(jPromPractica)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(37, 37, 37)
                                .addComponent(jLimpiar))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(142, 142, 142)
                        .addComponent(jLabel1)))
                .addContainerGap(46, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jCarnet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jPromTeoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jPromPractica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jEnviar)
                    .addComponent(jLimpiar))
                .addGap(37, 37, 37))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jNombreActionPerformed

    private void jNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jNombreKeyTyped
        Character l = evt.getKeyChar();

        if(!Character.isLetter(l) && l != KeyEvent.VK_SPACE)
        {
            evt.consume();
        }
    }//GEN-LAST:event_jNombreKeyTyped

    private void jCarnetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCarnetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCarnetActionPerformed

    private void jCarnetKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jCarnetKeyTyped
        Character s = evt.getKeyChar();

        if(!Character.isDigit(s))
        {
            evt.consume();
        }

    }//GEN-LAST:event_jCarnetKeyTyped

    private void jPromTeoriaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPromTeoriaKeyTyped

        if(!Character.isDigit(evt.getKeyChar()) && evt.getKeyChar() != '.')
        {
            evt.consume();
        }
        if(evt.getKeyChar() == '.' && jPromTeoria.getText().contains("."))
        {
            evt.consume();
        }
    }//GEN-LAST:event_jPromTeoriaKeyTyped

    private void jPromPracticaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPromPracticaKeyTyped

        if(!Character.isDigit(evt.getKeyChar()) && evt.getKeyChar() != '.')
        {
            evt.consume();
        }
        if(evt.getKeyChar() == '.' && jPromTeoria.getText().contains("."))
        {
            evt.consume();
        }
    }//GEN-LAST:event_jPromPracticaKeyTyped

    private void jEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jEnviarActionPerformed
        if(jCarnet.getText().equals("") || this.jNombre.getText().equals("") || this.jPromPractica.getText().equals("") || this.jPromTeoria.getText().equals("") ){
            JOptionPane.showMessageDialog(null,"¡Datos incompletos!","ERROR",0);

        }
        else
        {
           llenar();  
        }
       
    }//GEN-LAST:event_jEnviarActionPerformed

    private void jLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jLimpiarActionPerformed
        limpiar();
    }//GEN-LAST:event_jLimpiarActionPerformed

    private void jPromTeoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jPromTeoriaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jPromTeoriaActionPerformed
    public void limpiar()
    {
        this.jCarnet.setText(" ");
        this.jNombre.setText(" ");
        this.jPromPractica.setText(" ");
        this.jPromTeoria.setText(" ");
    }
    public void llenar()
    {
        Docente obj = new Docente();
        obj.setCarnet(Integer.parseInt(this.jCarnet.getText()));
        obj.setNombre(this.jNombre.getText());
        obj.setNotaPractica(Double.parseDouble(this.jPromPractica.getText()));
        obj.setNotaTeoria(Double.parseDouble(this.jPromTeoria.getText()));
        
        JOptionPane.showMessageDialog(null, "Datos del estudiante: " +
                                            "\n Carnet: " + obj.getCarnet() + 
                                            "\n Nombre: " + obj.getNombre() +
                                            "\n Nota Final: " + obj.calcularPromedio(obj.getNotaTeoria(), obj.getNotaPractica()));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField jCarnet;
    private javax.swing.JButton jEnviar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JButton jLimpiar;
    private javax.swing.JTextField jNombre;
    private javax.swing.JTextField jPromPractica;
    private javax.swing.JTextField jPromTeoria;
    // End of variables declaration//GEN-END:variables
}
