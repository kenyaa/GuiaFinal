
package com.clases;
/**
 * Nombre de la clase: Rectangulo
 * Fecha: 24/05/2018
 * Vrsion: 8.1
 * CopyRigth: Itca Fepade
 * @author Kenya Mejia
 * 
 */
public class Rectangulo extends Poligono{

    public Rectangulo() {
    }
    
    
    @Override
    public double Area(){
        double area=0;
        
        area=this.getLado1()*getLado2();
        
        return area;
    }
    
    
    
    
}
