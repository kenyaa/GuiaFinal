
package com.clases;
/**
 * Nombre de la clase: Poligono
 * Fecha: 24/05/2018
 * Vrsion: 8.1
 * CopyRigth: Itca Fepade
 * @author Kenya Mejia
 * 
 */
public abstract class Poligono {
    
    private double lado1;
    private double lado2;

    public Poligono() {
    }

    public Poligono(double lado1, double lado2) {
        this.lado1 = lado1;
        this.lado2 = lado2;
    }

    public double getLado1() {
        return lado1;
    }

    public void setLado1(double lado1) {
        this.lado1 = lado1;
    }

    public double getLado2() {
        return lado2;
    }

    public void setLado2(double lado2) {
        this.lado2 = lado2;
    }
        
   //metodo abstracto 
    public abstract double Area(); 
    
}
