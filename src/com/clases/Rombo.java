
package com.clases;
/**
 * Nombre de la clase: Rombo
 * Fecha: 24/05/2018
 * Vrsion: 8.1
 * CopyRigth: Itca Fepade
 * @author Kenya Mejia
 * 
 */
public class Rombo extends Poligono{
   
    public Rombo() {
    }

   @Override
   public double Area(){
       double area=0;
       //por medio de la diagonal mayor y menor
       
       area=(getLado1()*getLado2())/2;
       
       
       return area;
   }

   
    
    
    
    
}
