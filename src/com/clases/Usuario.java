package com.clases;

/**
 * Nombre de la clase: Usuario
 * Fecha: 24/05/2018
 * CopyRight: ITCA-FEPADE
 * Versión: 1.0
 * @author Kenya Mejia
 */
public class Usuario {
    private String nombre; 
    private int edad;
    private String estadoc;

    public Usuario(String nombre, int edad, String estadoc) {
        this.nombre = nombre;
        this.edad = edad;
        this.estadoc = estadoc;
    }

    public Usuario() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getEstadoc() {
        return estadoc;
    }

    public void setEstadoc(String estadoc) {
        this.estadoc = estadoc;
    }
    
    
}
