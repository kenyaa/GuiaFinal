
package com.clases;


/**
 * Nombre de la clase: Triangulo
 * Fecha: 24/05/2018
 * Vrsion: 8.1
 * CopyRigth: Itca Fepade
 * @author Kenya Mejia
 * 
 */
public class Triangulo extends Poligono {
 
    public Triangulo() {
    }

    
  @Override
    public double Area(){
        double area=0;
        area=(getLado1() * getLado2()) / 2;
        
        return area;
    }

   



  
    
    
    
    
}
