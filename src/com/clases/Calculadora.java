package com.clases;

/**
 * Nombre de la clase: Calculadora
 * Fecha: 24/05/2018
 * CopyRight: ITCA-FEPADE
 * Versión: 1.0
 * @author Kenya Mejia
 */
public interface Calculadora {
    
    public double suma();
    public double resta();
    public double multiplicacion();
    public double division();
    public double potencia();
    public double raiz();
}
