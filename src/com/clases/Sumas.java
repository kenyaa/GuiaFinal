package com.clases;

/**
 * Nombre de la clase: Sumas
 * Fecha: 24/05/2018
 * CopyRight: ITCA-FEPADE
 * Versión: 1.0
 * @author Kenya Mejia
 */
public class Sumas {
    
    public int sumar(int n1, int n2)
    {
        return n1+n2;
    }
    
    public double sumar(double n1, double n2)
    {
        
        return n1+n2;
    }
    
    public String sumar(String n1, String n2)
    {
        return n1+n2;
    }
}
