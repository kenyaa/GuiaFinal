/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clases;

/**
 *
 * @author Andrea
 */
public class Ejercicio2 {
    private double base;
    private double altura;
    private double dMayor;
    private double dMenor;
    
    public double rectangulo(double base, double altura)
    {
        double area = base * altura;
        return area;
    }
    public double triangulo(double base, double altura)
    {
        double area = (base * altura)/2;
        return area;
    }
    public double rombo(double dMenor, double dMayor)
    {
        double area = (dMenor * dMayor)/2;
        return area;
    }
    public double romboide(double base, double altura)
    {
        double area = base * altura;
        return area;
    }
    
}
