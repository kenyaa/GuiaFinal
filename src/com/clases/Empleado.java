package com.clases;

/**
 * Nombre de la clase: Empleado
 * Fecha: 24/05/2018
 * CopyRight: ITCA-FEPADE
 * Versión: 1.0
 * @author Kenya Mejia
 */
public class Empleado {
    private int carnet;
    private String nombre; 
    private int edad; 
    private double salario;
    private String genero;
    private String intereses;
    private String dui;
    private String nit;
    private int licencia;
    private String telefono;
    private String departamento;
    private String cargo; 
    private double sueldofinal;

    public Empleado() {
    }

    public Empleado(int carnet, String nombre, int edad, double salario, String genero, String intereses, String dui, String nit, int licencia, String telefono, String departamento, String cargo, double sueldofinal) {
        this.carnet = carnet;
        this.nombre = nombre;
        this.edad = edad;
        this.salario = salario;
        this.genero = genero;
        this.intereses = intereses;
        this.dui = dui;
        this.nit = nit;
        this.licencia = licencia;
        this.telefono = telefono;
        this.departamento = departamento;
        this.cargo = cargo;
        this.sueldofinal = sueldofinal;
    }

    public int getCarnet() {
        return carnet;
    }

    public void setCarnet(int carnet) {
        this.carnet = carnet;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getIntereses() {
        return intereses;
    }

    public void setIntereses(String intereses) {
        this.intereses = intereses;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public int getLicencia() {
        return licencia;
    }

    public void setLicencia(int licencia) {
        this.licencia = licencia;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public double getSueldofinal() {
        return sueldofinal;
    }

    public void setSueldofinal(double sueldofinal) {
        this.sueldofinal = sueldofinal;
    }
    
    public double calcularSalario()
    {
        double sueldof;
        
        if(this.salario>0 && this.salario<300)
        {
            sueldof=this.salario+(this.salario*0.10);
        }
        else if(this.salario>=300 && this.salario<600)
        {
            sueldof=this.salario+(this.salario*0.12);
        }
        if(this.salario>=600)
        {
            sueldof=this.salario+(this.salario*0.14);
        }
        else
        {
            sueldof=this.salario;
        }
        
        return sueldof;
    }
}
