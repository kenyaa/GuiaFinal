
package com.clases;
/**
 * Nombre de la clase: Romboide
 * Fecha: 24/05/2018
 * Vrsion: 1.0
 * CopyRigth: Itca Fepade
 * @author Kenya Mejia
 * 
 */
public class Romboide extends Poligono{
   
    public Romboide() {
    }
    
    @Override
    public double Area(){
        double area=0;
        area=getLado1()*getLado2();
        return area;
    }
    
    
    
    
    
    
}
